# **M169 - Services mit Containern bereitstellen**

[Modul 169 - Services mit Containern bereitstellen](https://www.modulbaukasten.ch/module/169/1/de-DE?title=Services-mit-Containern-bereitstellen) ist eine [Gitlab Static Website](https://docs.gitlab.com/ee/user/project/pages/) basierend auf [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/). Repository beinhaltet Markdown-Vorlagen für die Webseite sowie Terraform-Code für die im Unterricht behandelte Infrastruktur.

<img src="./assets/screenshot.png" alt="image-20230417080309023" style="width: 600px" />


Webseite unter https://bbwin.gitlab.io/m169-aws-fargate/fargate/

## Voraussetzungen

Benötigt **Python 3**, kann unter Windows Subshell for Linux 2, Linux und MacOS verwendet werden.

Eine virtuelle Umgebung für Python ist empfehlenswert. Unter Ubuntu mit folgendem Befehl installierbar

```sh
sudo apt update && sudo apt install python3-venv
```

## Quick start

Um die Dateien lokal bearbeiten zu können:

1. Klon des Repository `git clone git@gitlab.com:bbwin/m169-aws-fargate.git && cd m169-aws-fargate ` 

2. Aktivieren der virtuellen Umgebung
   ```sh
    [ -d .venv ] && source .venv/bin/activate || python3 -m venv .venv && source .venv/bin/activate
   ```

3. Installieren der mkdocs Abhängigkeiten mittels `pip`: `pip install -r requirements.txt`. 

4. Live preview der Unterlagen: `mkdocs serve`, Seite aufrufbar unter `http://localhost:8000`

## mkdocs Ordnerstruktur

Konfiguration von mkdocs geschieht über die Datei `mkdocs.yml`. Navigation wird über `nav` gesteuert

```yaml
nav:
  - Home: fargate/index.md
  - 20 Fargate:
    - fargate/index.md
    - fargate/local.md
    - fargate/ecr.md
    - fargate/ecs.md
    - fargate/deploy.md
  - 30 Infrastructure as Code:
    - 30.1 Übersicht: iac/index.md
    - iac/git.md
```

Markdown Dokumente im Ordner `docs`. Hilfestellung unter [Reference - Material for MkDocs](https://squidfunk.github.io/mkdocs-material/reference/).

`[gitlab-ci.yml](.gitlab-ci.yml)` übernimmt die Erstellung der Webseite mittels Build Pipeline. 

## Terraform infrastructure

1. Install `terraform`, follow [HashiCorp Official Packaging Guide](https://www.hashicorp.com/official-packaging-guide)
   ```sh
   sudo apt update && sudo apt install -y gpg
   wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
   echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
   sudo apt update
   sudo apt install -y terraform
   ```

2. Install [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
   ```sh
   curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
   unzip awscliv2.zip
   sudo ./aws/install
   ```

3. Clone this project `git clone https://gitlab.com/bbwin/m169-aws-fargate.git && cd m169-aws-fargate`

4. Start [AWS Academy Learner Lab](https://awsacademy.instructure.com), select **AWS Details** and copy the **Cloud Access** credentials into your local `~/.aws/credentials` file
   <img src="./assets/image-20230403094132515.png" alt="image-20230403094132515" style="zoom: 67%;" />

5. `terraform init` to initialize Terraform

6. `terraform plan` to see planned ressources

7. `terraform apply` to create resources

8. Run `./push_commands.sh` to login to docker, build image, push to newly created repository and force new deployment

## Links

- [Terraform: How to use Dynamic Blocks when Conditionally Deploying to Multiple Environments | by Andrea Marinaro | Geek Culture | Medium](https://medium.com/geekculture/terraform-how-to-use-dynamic-blocks-when-conditionally-deploying-to-multiple-environments-57e63c0a2b56)
- [Terraform tips & tricks: loops, if-statements, and gotchas | by Yevgeniy Brikman | Gruntwork](https://blog.gruntwork.io/terraform-tips-tricks-loops-if-statements-and-gotchas-f739bbae55f9)
- [Best practices for using Terraform  |  Google Cloud](https://cloud.google.com/docs/terraform/best-practices-for-terraform)
- [Auto generate documentation from Terraform modules - DEV Community](https://dev.to/pwd9000/auto-generate-documentation-from-terraform-modules-42bl)
- [terraform-docs/terraform-docs: Generate documentation from Terraform modules in various output formats](https://github.com/terraform-docs/terraform-docs)
- [Terraform .tfvars files: Variables Management with Examples](https://spacelift.io/blog/terraform-tfvars)
- [silinternational/ecs-deploy: Simple shell script for initiating blue-green deployments on Amazon EC2 Container Service (ECS)](https://github.com/silinternational/ecs-deploy)
- [update-service — AWS CLI 1.27.105 Command Reference](https://docs.aws.amazon.com/cli/latest/reference/ecs/update-service.html)
