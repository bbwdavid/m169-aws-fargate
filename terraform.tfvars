repository_name      = "nginx-custom"
cluster_name         = "fargate-cluster"
task_definition_name = "nginx-custom"
service_name         = "fargate-service"
enable_load_balancer = false
