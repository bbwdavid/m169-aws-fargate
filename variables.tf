variable "repository_name" {
  type        = string
  description = "The name of the Elastic Container Registry repository."
  default     = "nginx-custom"
}

variable "cluster_name" {
  type        = string
  description = "Name of the Elastic Container Service cluster."
  default     = "fargate-cluster"
}

variable "task_definition_name" {
  type        = string
  description = "Unique name for the Elastic Container Service task definition."
  default     = "fargate-task-definition"
}

variable "container_name" {
  type        = string
  description = "Name of the Elastic Container Service container definition"
  default     = "fargate-container"
}

variable "service_name" {
  type        = string
  description = "Name of the Elastic Container Service service"
  default     = "fargate-service"
}

variable "enable_load_balancer" {
  type        = bool
  description = "Use a load balancer with your Elastic Container Service service"
  default     = false
}