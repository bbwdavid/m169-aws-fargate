provider "aws" {
  region  = "us-west-2" # "us-east-1" or "us-west-2"
  profile = "default"
}

locals {
  container_port = 80
}

# Used region
data "aws_region" "current" {}

# Fetch id of default VPC and save as aws_vpc.default
data "aws_vpc" "default" {
  default = true
}

# Get available zones for used region
data "aws_availability_zones" "available" {
  state = "available"
}

# Fetch id of IAM role LabRole and save as aws_iam_role.labrole
data "aws_iam_role" "labrole" {
  name = "LabRole"
}

# Create a private ECR repository
resource "aws_ecr_repository" "main" {
  name = var.repository_name

  # delete the repository even if it contains images
  force_delete = true
}

# Create an ECS cluster
resource "aws_ecs_cluster" "main" {
  name = var.cluster_name
}

# Use Fargate 
resource "aws_ecs_cluster_capacity_providers" "main" {
  cluster_name = aws_ecs_cluster.main.name

  capacity_providers = ["FARGATE", "FARGATE_SPOT"]
}

# Create a task definition
resource "aws_ecs_task_definition" "main" {
  family                   = var.task_definition_name
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512

  # Use LabRole for ECS to work in AWS Academy Learner Lab
  execution_role_arn = data.aws_iam_role.labrole.arn
  task_role_arn      = data.aws_iam_role.labrole.arn

  # Required for FARGATE
  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }

  container_definitions = jsonencode([
    {
      name  = var.container_name
      image = "${aws_ecr_repository.main.repository_url}:latest"
      portMappings = [
        {
          containerPort = local.container_port
          hostPort      = local.container_port
        }
      ]
    }
  ])
}

# Create a service that uses the task definition
resource "aws_ecs_service" "main" {
  name            = var.service_name
  cluster         = aws_ecs_cluster.main.arn
  task_definition = aws_ecs_task_definition.main.arn
  launch_type     = "FARGATE"
  desired_count   = 1

  dynamic "load_balancer" {
    for_each = var.enable_load_balancer == true ? [1] : []
    content {
      target_group_arn = aws_lb_target_group.main[0].arn
      container_name   = var.container_name
      container_port   = local.container_port
    }
  }

  network_configuration {
    security_groups  = [aws_security_group.main.id]
    assign_public_ip = true
    subnets          = [aws_default_subnet.primary.id, aws_default_subnet.secondary.id]
  }

  depends_on = [aws_lb.main[0]]
}


# Create a security group for the service
resource "aws_security_group" "main" {
  name_prefix = "my-security-group"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    from_port   = local.container_port
    to_port     = local.container_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow outbound communication to pull ECR images
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

# Resource to manage a the primary subnet in the current region
# Creates it only when not existing
resource "aws_default_subnet" "primary" {
  availability_zone = data.aws_availability_zones.available.names[0]
}
resource "aws_default_subnet" "secondary" {
  availability_zone = data.aws_availability_zones.available.names[1]
}

resource "aws_lb_target_group" "main" {
  count = var.enable_load_balancer ? 1 : 0

  name        = "tf-example-lb-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = data.aws_vpc.default.id

  depends_on = [aws_lb.main[0]]
}

resource "aws_lb" "main" {
  count = var.enable_load_balancer ? 1 : 0

  name               = "example-lb"
  internal           = false
  load_balancer_type = "application"

  subnets = [aws_default_subnet.primary.id, aws_default_subnet.secondary.id]

  security_groups = [aws_security_group.main.id]
}

resource "aws_lb_listener" "main" {
  count = var.enable_load_balancer ? 1 : 0

  load_balancer_arn = aws_lb.main[0].arn
  port              = local.container_port
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.main[0].arn
  }
}