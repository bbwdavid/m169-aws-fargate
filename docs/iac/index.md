# 30.1 Infrastructure as Code

!!! abstract "Kompetenzen"

    - Ich kann **Service-Konfigurationen** codebasiert in Containern bereitstellen und mit **Infrastructure-as-Code** (IaC) dokumentieren
    - Ich kann das **Versionsverwaltungssystem GIT** zur Verwaltung und Erstellung von versioniertem, kommentierten Code anwenden
    - Ich kann **Sicherheitsanforderungen** umsetzen und auf ihre **Wirksamkeit** prüfen

## Einleitung

Erinnern Sie sich zurück an die vorhergehenden Aufgaben in denen Sie anhand von Textdateien gewisse Prozesse automatisiert haben:

- **Spring Boot Build Prozess mit `pom.xml`**

	Die `pom.xml`-Datei enthält die Konfiguration für das Maven-Projekt und definiert die Build-Phasen, Abhängigkeiten und Plugins, die für das Erstellen und Verwalten des Projekts benötigt werden. Hier ein Auszug aus der Datei aus dem [Repository m347-ref-card-01](https://gitlab.com/bbwrl/m347-ref-card-01/-/tree/main/)

	```xml
	--8<-- "https://gitlab.com/bbwrl/m347-ref-card-01/-/raw/main/pom.xml:2:12"
	```

	Stellen Sie sich vor, Sie müssten Maven für jeden Build eigenhändig mit den nötigen Befehlen ausstatten: `maven compile --modelVersion=4.0.0 --group-id=ch.bbw --artifactId=app-refcard-01`. Das wäre mühselig und fehleranfällig. Die Umsetzung mit einer `pom.xml` ermöglicht den universalen Einsatz von `mvn package`, alles andere wird von Maven direkt aus `pom.xml` ausgelesen.

- **Docker Build Prozess mit `Dockerfile`**

	Ähnlich funktioniert auch Docker. Anstatt einer langen Abfolge von vielen Befehlen im Stil von

	- `docker run -it ubuntu bash`
	- `apt update && apt install nginx`
	- `docker commit`

	können wir alle Anweisungen in einem `Dockerfile` abbilden

	```Dockerfile
	FROM ubuntu
	RUN apt update && apt install nginx
	```

	Der Befehl `docker build .` führt alle Anweisungen selbstständig durch.

## Was ist Infrastructure as Code?

Wir übernehmen die Praktiken aus der Softwareentwicklung und behandeln **Infrastruktur und Tools** wie **Software-Sourcecode und Daten**.  Wir definieren deshalb unsere Infrastruktur und deren Konfiguration nur noch in Code und automatisieren wiederholbare Abläufe für die Bereitstellung und Änderung von Systemen und deren Konfiguration.

Dieser neuere Ansatz von **Infrastructure as Code** erlaubt uns zudem, moderne Entwicklungstools und Praktiken zu verwenden:

- Versionskontrolle mit **Git**
- Test-getriebene Entwicklung (test-driven development **TDD**)
- Kontinuierliche Integration (continuous integration **CI**)
- Kontinuierliche Bereitstellung (continuous delivery **CD**)
