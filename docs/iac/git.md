# 30.2 Versionsverwaltung mit Git

!!! abstract "Zielsetzung"

    Sie erstellen einen **GitLab Account**, kopieren das **Ref-Card-03 Projekt**, erstellen ein **Dockerfile**, verfassen eine informative **`README.md`** und exkludieren nicht gewünschte Dateien mit einer `.gitignore`

Versionsverwaltungssysteme werden verwendet, um Änderungen an Dateien zu verfolgen. Dabei werden die Dateien in sogenannten Repositories (engl. Behälter, Aufbewahrungsorte) abgelegt. Sie hatten in vorhergehenden Modulen bereits mit Git gearbeitet, wir beschränken uns daher in diesem Modul auf die Anwendung.

## Auftrag
- Erstellen Sie sich einen [Gitlab-Account](#gitlab-repository)
- Forken Sie [Ref-Card-03](https://gitlab.com/bbwin/ref-card-03)
- Erstellen Sie ein [Dockerfile](#) für das Spring Boot Projekt.
- Erstellen Sie ein informatives [`README.md`](#readmemd) für das Projekt
- Lassen Sie sich eine [`.gitignore`](#gitignore) Datei generieren
- Schreiben Sie sinnvolle [Commit-Messages](#commit-strategien) für diese Dateien. Vergessen Sie den `push` ins Remote Repository nicht.

## Gitlab Repository

1. Erstellen Sie sich einen **Account** bei [GitLab](https://gitlab.com/users/sign_up). 

	!!! info

		Verwenden Sie Ihre BBW-Adresse **vorname.nachname@lernende.bbw.ch** für das Login.

2. Erstellen Sie einen Fork des [bbwin / Ref Card 03 Projekt ](https://gitlab.com/bbwin/ref-card-03)

	![image-20230416134913203](./assets/image-20230416134913203.png)

	Ein **Fork** ist eine exakte Kopie des Ursprungsprojekts. Ab dann entwickelt es sich unabhängig; man entfernt, verändert oder ergänzt den Quellcode so, wie man es für nötig hält. Es ist möglich, aber eher unwahrscheinlich, dass ein Fork wieder mit seinem Ursprungsprojekt zusammengeführt wird.

## Wichtige Dateien

Dem  [bbwin / Ref Card 03 Projekt ](https://gitlab.com/bbwin/ref-card-03) fehlen ein paar elementare Anpassungen oder Dateien.

### Dockerfile

Wie in den vorhergehenden Übungen, benötigen Sie auch hier ein `Dockerfile` um das Projekt **Ref-Card-03** als Container betreiben zu können. Sie können als Vorlage das `Dockerfile` vom Auftrag [20 Fargate](fargate/) verwenden und anpassen.

!!! warning "Java Version"

	Die **Ref-Card-01** aus der vorherigen Übung verwendete Java **17**. Überprüfen Sie die `<java.version>` in der Datei  [pom.xml](https://gitlab.com/bbwin/ref-card-03/-/blob/main/pom.xml). Passen Sie Ihr `Dockerfile` dementsprechend an.

### README.md

Jedes Repository sollte ein `README.md` enthalten. Der Zweck eines README ist es, folgende 3 Fragen innert kürzester Zeit zu beantworten:

1. Was soll mit diesem Projekt erreicht werden? (Kurze Beschreibung)
2. Kann ich es benutzen? (Was sind die Voraussetzungen?)
3. Wenn ja, wie? (Wie kann ich es installieren? Was sind die Code-Snippets, die sofort funktionieren?)

Solche README-Dateien werden mit [Markdown](https://www.heise.de/mac-and-i/downloads/65/1/1/6/7/1/0/3/Markdown-CheatSheet-Deutsch.pdf) geschrieben (daher die Dateiendung `.md`). Ähnlich wie HTML ist Markdown eine **Auszeichnungssprache**, eine maschinenlesbare Sprache für die Gliederung und Formatierung von Texten.


1. **Projektbeschreibung**

	Bauen Sie die Projektbeschreibung mit folgenden Textbausteinen zusammen, wobei die Begriffe in den eckigen Klammern als Platzhalter dienen:

	`<project_name>` is a `<utility/tool/feature>` that allows `<insert_target_audience>` to do `<action/task_it_does>`.

	Beispiele:
	!!! quote "[Tensorflow](https://www.tensorflow.org/overview/)"

		TensorFlow makes it easy for beginners and experts to create machine learning models. See the sections below to get started.

	!!! quote "[PyTorch](https://pytorch.org/)"

		An open source machine learning framework that accelerates the path from research prototyping to production deployment.

	!!! quote "[Kubernetes](https://kubernetes.io/)"

		Kubernetes is an open-source system for automating deployment, scaling, and management of containerized applications.
	
2. **Vorraussetzungen**

	Der Abschnitt Voraussetzungen besteht aus drei Kategorien.

	- **Betriebssystem**: Das Betriebssystem kommt zuerst, weil es für die meisten Benutzer vorgegeben ist. Falls ein Projekt ausschlisslich auf macOS läuft gibt es keinen Grund für einen Windows-Benutzer, sich noch weiter mit diesem Projekt zu befassen.
	- **Framework Infrastruktur**: Frameworks stellen auch ein potenzielles Kompatibilitätsproblem dar. Falls das Projekt ausschliesslich mit Python 2 funktioniert, muss sich der Benutzer entscheiden, ob er diesen erheblichen Aufwand betreiben will um das Projekt zu nutzen.
	- **Wissen**: Wie viel Wissen muss sich zur Nutzung dieses Projekts angeeignet werden?

3.  **Installation und Verwendung**

	- Einfache Copy-Paste Installationsanweisung. Sollte in unter 5min umsetzbar sein und keine komplexen Abhängigkeiten haben
	- Anschliessend die Verwendung des Projekts anhand so vielen Beispielen wie möglich demonstrieren. Das erste Beispiel sollte im Stil eines "hello-world" so einfach wie möglich gehalten werden und einfach laufen. Weitere Beispiele können weiterführende, komplizierte Anwendungsfälle zeigen.

!!! tip "Gute README.md Vorlage"
	[scottydocs/README-template.md: A README template for anyone to copy and use.](https://github.com/scottydocs/README-template.md)

### .gitignore

Nicht jede Datei in Ihrem Projekt sollte von Git nachverfolgt werden. Temporäre Dateien aus Ihrer Entwicklungsumgebung, Testausgaben und Logs sind alles Beispiele für Dateien, die wahrscheinlich nicht nachverfolgt werden müssen. Sie können Git mitteilen, dass bestimmte Dateien in Ihrem Projekt nicht nachverfolgt werden, indem Sie eine `.gitignore` Datei hinzufügen und konfigurieren.

!!! tip ".gitignore Generator"
	[gitignore.io - Create Useful .gitignore Files For Your Project](https://www.toptal.com/developers/gitignore)


### .gitlab-ci.yml

Mit einer `.gitlab-ci.yml` Datei können Sie in Gitlab eine CI/CD Pipeline festlegen. Dies wird im nächsten Schritt dann wichtig für unser Projekt.

## Git Commit

Sobald Sie ihre ersten Dateien im Ordner hinzugefügt haben, sollten Sie die Änderungen commiten und in Ihr Remote Repository pushen.

![Git - Working area](./assets/git-working-area.png)

Ob Sie dies im Terminal oder in Visual Studio Code umsetzen ist Ihnen überlassen. Zu Beginn ist das Modul **Source Control** in Visual Studio Code jedoch sehr zu empfehlen, es ermöglicht Ihnen mittels einer grafischen Oberfläche alle Ihre Änderungen auf einen Blick zu sehen.

![Visual Studio Code - Source Control](./assets/vscode-source-control.png)

### Commit Strategien

Bevor wir uns mit dem Verfassen von Git-Commit-Nachrichten befassen, wollen wir zunächst kurz auffrischen, was ein Commit in Git ist.

Es kann hilfreich sein, sich einen Commit als Checkpoint oder Savepoint für Ihr Projekt vorzustellen. In vielen Videospielen werden Checkpoints erreicht und der Fortschritt gespeichert, nachdem eine bestimmte Handlung oder Herausforderung gemeistert wurde. In ähnlicher Weise wird ein Git-Commit in der Regel durchgeführt, nachdem Sie einen Meilenstein erreicht haben und Sie Ihren Fortschritt speichern möchten. 

Beim Verfassen von Git-Commits sollten Sie ein paar bewährte Praktiken einhalten:

- Wählen Sie den **Imperativ** (Befehlsform) - "Halten Sie", "Steigen Sie ein", Schnallen Sie sich an"
  `fix`, `add`, `remove`, `change`
- Halten Sie sich **kurz**: Eine commit-Nachricht sollte nicht länger als **50 Zeichen** sein.
- Versetzen Sie sich in sich selbst: Welche **Informationen** sind nötig um diesen Commit zu verstehen? 

!!! success "Gutes Beispiel"
	```
	fix issue with login button not showing
	```

!!! failure "Schlechtes Beispiel"
	```
	fixed bug
	```

---

!!! success "Zusammenfassung"
	In Ihrem persönlichen GitLab-Account liegt ein Fork des **Ref-Card-03** Projekts. Zum bereits vorhandenen Sourcecode und den Maven-Dateien, haben Sie sich ein `Dockerfile`, `README.md` sowie eine `.gitignore` erstellt. Diese drei Dateien haben Sie erfolgreich zu Ihrem Repository hinzugefügt und mit einer sinnvollen Commit-Nachricht versehen.

## Weiterführende Links

- [Git Pro - Book](https://git-scm.com/book/en/v2)
- [README best practices. Don’t spend 2 hours on your next… | by Tianhao Zhou | Medium](https://tianhaozhou.medium.com/readme-best-practices-7c9ad6c2303)
- [Git Best Practices – How to Write Meaningful Commits, Effective Pull Requests, and Code Reviews](https://www.freecodecamp.org/news/git-best-practices-commits-and-code-reviews/)
- [Write Better Commits, Build Better Projects | The GitHub Blog](https://github.blog/2022-06-30-write-better-commits-build-better-projects/)
