# 30.4 CI/CD-Pipeline

!!! abstract "Zielsetzung"

    Sie verstehen die Elemente einer CI/CD-Pipeline. Sie automatisieren den Container-Build und lassen die Applikation anschliessend in Ihrer AWS Umgebung laufen.

CI/CD (Continuous Integration/Continuous Delivery) sind bewährte DevOps-Methoden zur Automatisierung in der Anwendungsentwicklung. Die Hauptkonzepte von CI/CD sind **Continuous Integration** (kontinuierliche Integration), **Continuous Delivery** und **Continuous Deployment** (kontinuierliche Verteilung).

## Pipeline-Phasen

Die Schritte in einer CI/CD-Pipeline stellen verschiedene Untergruppen von Aufgaben dar, die in sogenannte Pipeline-Phasen eingeteilt werden. Zu diesen Phasen gehören üblicherweise:

- **Build**: Die Phase, in der die Anwendung kompiliert wird.
- **Test**: Die Phase, in der der Code getestet wird. Hier lassen sich durch Automatisierung sowohl der Zeit- als auch der Arbeitsaufwand verringern.
- **Release**: Die Phase, in der die Anwendung ins Repository gestellt wird.
- **Bereitstellung**: In dieser Phase wird der Code in der Produktionsumgebung bereitgestellt.
- **Validierung und Compliance** – Welche Schritte zur Validierung eines Builds notwendig sind, bestimmen die Anforderungen des jeweiligen Unternehmens.

Diese Liste der Pipeline-Phasen ist nicht abschliessend. So werden Sie Ihre eigene Pipeline an den Anforderungen Ihres Unternehmens und der Applikation ausrichten müssen.

## Pipelines in GitLab

In einem ersten Schritt setzen Sie in GitLab eine Pipeline für die Phasen "Build" und "Release" um. Das Ziel ist die automatisierte Kompilierung und Bereitstellung des Container-Images direkt aus GitLab.

In GitlLab besteht eine Pipeline aus zwei Komponenten:

- **Jobs**: Definieren das *was*. Ein Job kann beispielsweise kompilieren oder Code testen.
- **Stages**: Definieren *wann* ein Job ausgeführt wird. Eine Stage B, die Tests ausführt, wird nach der Stage A, welche kompiliert, ausgeführt.

![Pipelines example](./assets/gitlab-stages.png)

## Auftrag

- Erstellen Sie eine GitLab Pipeline
- Bauen Sie eine Stage Release in Ihre Pipeline
- Erstellen Sie die CI/CD Variablen
- Testen Sie Ihre Pipeline

### Vorarbeiten für GitLab-Runner
Damit Sie Ihren persönlichen GitLab-Account nicht mit einer Kreditkarte verifizieren müssen (was nötig wäre, um die offiziellen GitLab-Runner als Ressourcen für die Builds zu nutzen), setzen Sie einen GitLab-Runner bei sich lokal in Docker auf.
Erstellen Sie einen Verweis zu Ihrem eigenen GitLab-Runner unter **Settings** - **CI/CD** - **Runners**. Stellen Sie sicher, dass die Shared Runners für Ihre Pipelines nicht genutzt werden und erstellen Sie dann einen neuen Project runner.

![image-20230510084245151](./assets/gitlab-runner-create1.png)

![image-20230510084245154](./assets/gitlab-runner-create2.png)

Nach Erstellen des Runners erhalten Sie einen Token für diesen. Kopieren Sie den Token in den unten stehenden Befehl und führen Sie alle bei sich lokal in der Shell aus.

```bash
docker run -d --name gl-runner -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner

docker exec gl-runner gitlab-runner register --url https://gitlab.com \
  --token HIER_IHREN_TOKEN_EINFUEGEN --executor docker \
  --docker-image docker:23.0.4 --docker-privileged --non-interactive

docker restart gl-runner
```
Verifizieren Sie danach, dass GitLab und Ihr lokaler Runner verbunden sind:

![image-20230510084245152](./assets/gitlab-runner-connected.png)

### Simple GitLab Pipeline

Wir starten mit einer ganz einfachen Pipeline, um die Funktionalität zu testen.

Erstellen Sie in Ihrem Fork des **Ref-Card-03** Projekts die Datei `.gitlab-ci.yml` mit folgendem Inhalt.

```yaml title=".gitlab-ci.yml"
test-job:
  script:
    - echo "This is my first job!"
    - date
```

Commiten und pushen Sie diese Datei zu GitLab. Sobald Sie erfolgreich gepusht haben, finden Sie in GitLab jeweils unter **CI/CD** - **Pipelines** Ihre eigene Pipeline, welche sogleich von einem GitLab Runner ausgeführt wird.

![image-20230510084245512](./assets/gitlab-pipeline.png)

Erkunden Sie diese Pipeline und schauen Sie sich auch das Job-Log an. Recherchieren Sie dazu auch nach weiterführenden Erklärungen zu diesem Vorgang. Wenn Sie sich sattelfest fühlen und das Grundkonzept verstanden haben, fahren Sie mit dem nächsten Schritt weiter.

### Komplexere GitLab Pipeline für RefCard03

Jetzt ersetzen wir die zuvor erstellte Datei `.gitlab-ci.yml` mit Anweisungen zu unserer Applikation.

```yaml title=".gitlab-ci.yml"
image: docker:23.0.4

variables:
  DOCKER_HOST: tcp://docker:2375
  DOCKER_TLS_CERTDIR: ""

services:
  - docker:23.0.4-dind

package:
  stage: build
  before_script:
    - apk add --no-cache py3-pip
    - pip install awscli
    - aws --version

    - aws ecr get-login-password | docker login --username AWS --password-stdin $CI_AWS_ECR_REGISTRY

  script:
    - docker build --cache-from $CI_AWS_ECR_REGISTRY/$CI_AWS_ECR_REPOSITORY_NAME:latest -t $CI_AWS_ECR_REPOSITORY_NAME .
    - docker tag $CI_AWS_ECR_REPOSITORY_NAME:latest $CI_AWS_ECR_REGISTRY/$CI_AWS_ECR_REPOSITORY_NAME:latest
    - docker push $CI_AWS_ECR_REGISTRY/$CI_AWS_ECR_REPOSITORY_NAME:latest
```

Diese Pipeline ist bereits etwas komplexer und benutzt das GitLab CI/CD, um Docker Images mit Docker zu builden - auch DinD (Docker in Docker) genannt. Der erste Abschnitt mit `image:`, `variables:` und `services:` sind Voraussetzung und Sie sollten diese 1:1 so übernehmen. Falls Sie interessiert sind, finden Sie weiterführende Informationen dazu unter [Use Docker to build Docker images](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html).

Die Befehle unter `before_script:` und `script:` sollten Ihnen aber sehr bekannt vorkommen. Studieren Sie die Befehle! Welche sind neu, welche angepasst?

Commiten und pushen Sie diese aktualisierte Datei und überprüfen Sie den Status. Zwei wichtige Teile fehlen noch: die benutzten **Variablen** und die AWS Infrastruktur.

### CI/CD Variablen

Wir benutzen in dieser YAML-Datei mehrere nicht definierte Variablen. Erfassen Sie diese in GitLab unter **Settings** - **CI/CD** - **Variables**.

![image-20230510091802691](./assets/gitlab-variables.png)

Die Zugangsdaten finden Sie im Learner Lab unter **AWS Details** - **AWS CLI**. 

![image-20230510092304455](./assets/learnerlab-credentials.png)

!!! warning "Kurzlebige Secrets"

    Die Zugangsdaten im AWS Academy Learner Lab sind kurzlebig und erneuern sich bei jedem Neustart der AWS-Umgebung. Sie müssen diese Daten jeweils neu vom Learner Lab zu Gitlab übertragen, wenn Sie Ihr Lab starten.

Die weiteren Variablen sind von Ihrer Umgebung in AWS abhängig. Erstellen Sie eine ECR Registry (dies kennen Sie bereits aus vergangenen Aufträgen) und befüllen Sie die Variablen mit Ihren Werten:

- AWS_DEFAULT_REGION
- CI_AWS_ECR_REGISTRY
- CI_AWS_ECR_REPOSITORY_NAME

Starten Sie die Pipeline manuell aus GitLab und verfolgen Sie den Build-Prozess. Überprüfen Sie das erfolgreiche pushen in Ihr neu erstelltes ECR Repository.

![image-20230510092304451](./assets/gitlab-pipeline-pushed-to-ecr.png)

## Deployment einrichten
Mit den Schritten bis hierhin haben Sie nun eine Pipeline, die bei jedem Push in das Repository, das Image neu bildet und in ECR bereitstellt. Als letzter Schritt fehlt noch das automatische Deployment in ECS. Erstellen Sie hierfür in der Pipeline eine weitere Stage *auto-deploy* und versehen Sie diese mit den nötigen Befehlen, um mit der aws-cli das Deployment zu automatisieren. Vergessen Sie nicht, dass Sie zuvor (wie in vergangenen Aufträgen gelernt) den für den Betrieb nötigen Task, Service und Cluster im AWS Backend noch erstellen müssen.  

Aktualisieren Sie danach Ihr Readme.md mit allen neuen Informationen, so dass Ihr Repository (nach den IaC-Prinzipen) selbsterklärend ist.

## Rückblende

Verwenden Sie ein Blatt Papier und zeichnen Sie in Zweier-Gruppen grafisch auf, was Sie wie nun automatisiert haben. Starten Sie beim Java-Code, der in der Ref-Card-03 verarbeitet wird, bis zum fertig ausgelieferten Dienst, der in der AWS-Umgebung läuft. Zeichnen Sie schematisch auf, welche Daten durch welche Prozesse auf welcher Infrastruktur einflossen, um das Ergebnis letztlich zu erreichen. Diskutieren Sie auch die verwendete Symbolik!
