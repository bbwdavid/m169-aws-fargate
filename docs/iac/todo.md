**Notizen DRU**

Zielsetzung in diesem Block:

- Git-Repository erstellen (git init, Wichtige Dateien README.md / .gitignore, basis README.md, sinnvolle Commit-Nachrichten, commit often), anschliessend Wechsel in vscode
  - nur main branch, keine strategies, kein merge 
- ref-card-3 analog zu ref-card-1 containerisieren (Dockerfile), neue Registry in AWS erstellen und pushen
- docker compose, Grundlagen, MariaDB mit Spring verbinden, Secrets mit ENV Variablen
  - https://docs.aws.amazon.com/AmazonECS/latest/developerguide/secrets-envvar.html
  - ENV vars or secrets in docker? https://docs.aws.amazon.com/AmazonECS/latest/developerguide/secrets-envvar.html
- CI/CD mit gitlab. 
  - https://docs.gitlab.com/ee/ci/cloud_deployment/ecs/deploy_to_aws_ecs.html



![Git Commit](https://cbea.ms/content/images/size/w2000/2021/01/git_commit_2x.png)
