# 20.4 Aktualisiertes Docker Image

!!! abstract "Zielsetzung"

    - Sie aktualisieren Ihr Docker Image und können Ihre neue Version in AWS ECS erneuern.
    - Sie lösen das IP-Problem in AWS ECS

Ihr Container läuft nun mit einer öffentlichen IP in AWS. Sie stören sich aber an der Hintergrundfarbe der Webseite und möchten deshalb eine neue Version Ihres `nginx-custom` erstellen.

Öffnen Sie dazu die Datei `index.html` in einem Editor (`nano index.html` oder falls Sie die [Visual Studio Code Remote Development Extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack) aktiviert haben indem Sie das Verzeichnis mit `code .` in VS Code öffnen). Wählen eine neue Hintergrundfarbe und ersetzen Sie den Wert `<body bgcolor="CornflowerBlue">` mit der gewählten Farbe. Eine Auswahl an möglichen HTML-Farben finden Sie hier: [HTML Color Names](https://www.w3schools.com/colors/colors_names.asp)

Wiederholen SIe die **Push commands for nginx-custom** aus dem Abschnitt **Amazon Elastic Container Registry**.

```sh
docker build -t nginx-custom .
docker tag nginx-custom:latest 12345EXAMPLE.dkr.ecr.us-east-1.amazonaws.com/nginx-custom:latest
docker push 12345EXAMPLE.dkr.ecr.us-east-1.amazonaws.com/nginx-custom:latest
```

!!! tip "Dokumentation"
	Falls Sie nun zurück in zu AWS ECR gehen um die Befehle zu kopieren, sollten Sie spätestens jetzt beginnen, Ihre Schritte an einem Ort Ihrer Wahl zu dokumentieren. Ob OneNote, Markdown Editor oder in einem Word Dokument ist Ihnen überlassen, aber Sie sparen sich so wertvolle Zeit und es hilft Ihnen beim lernen.

In Ihrem private Repository in **AWS ECR** finden Sie nun zwei Images.
![image-20230328174958641](./assets/image-20230328174958641.png)

Überlegen Sie sich nun, wo Sie **Amazon Elastic Container Service** anweisen können, das neue Image in der Registry zu verwenden. Gehen Sie dazu die Schritte aus der obigen Anleitung nochmals durch, in welchem Abschnitt haben Sie das Image und den Tag angegeben? Wie viele Schritte sind nötig?

Kontaktieren Sie Ihren Lehrer falls Sie nicht weiterkommen.

!!! warning "Hinweis"
	Falls Sie die richtigen Schritte unternommen haben ändert sich ihre Public IP!

Wie lösen Sie das Problem mit den sich ständig wechselnden IP's?
