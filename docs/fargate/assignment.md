# Arbeitsauftrag

Während der nächsten Wochen arbeiten Sie an der Fallstudie App Ref. Card 01. Die meisten Kompetenzen, um diese zu erarbeiten, haben Sie bisher im Modul erwerben können. Die Fallstudie ist fachlich korrekt zu dokumentieren und die Dokumentation wird bewertet (Note mit doppeltem Gewicht). Führen Sie die Dokumentation sauber und strukturiert.

Das Bewertungsraster wird zu gegebener Zeit hier veröffentlicht. 

Der Arbeitsauftrag ist wahlweise als Einzelarbeit oder Gruppenarbeit (max. 2 Personen) zu lösen. 

## Fallstudie App Ref. Card 01

- Erstellen Sie Grafiken und Diagramme von **ECS** und **ECR**:  Wie spielen Repository, Task Definition, Task und Service zusammen
- Beschreiben Sie die Funktionalität von **ECS**
- Erstellen Sie eine ein privates Repository in **ECR** für die App Ref. Card 01. Benutzen Sie Ihr optimiertes `Dockerfile` aus dem Unterricht. Sollten Sie kein lauffähiges `Dockerfile` besitzen, verwenden Sie die Vorlage aus dem **bbwin/m346-ref-card-01** Repository.
	```dockerfile title="Dockerfile"
	FROM maven:3-openjdk-17-slim
	COPY src /src
	COPY pom.xml /
	RUN mvn -f pom.xml clean package
	RUN mv /target/*.jar app.jar
	ENTRYPOINT ["java","-jar","/app.jar"]
	```
- Erstellen und dokumentieren sie einen **ECS Cluster**. Achten Sie auf sinnvoll benannte Objekte, verwenden Sie keine generischen Namen mehr (Beispiel: Kein `nginx-custom` oder `fargate-service`)
- Erstellen Sie eine **Task Definition** für App Ref. Card 01. Recherchieren oder testen Sie die Minimalanforderungen an Prozessor und Speicher. 
- Befassen Sie sich mit den **VPC Security Groups**. Verwenden Sie für die Fallstudie nicht die `Default Security Group`, sondern erstellen Sie eine neue, sauber benannte Security Group und sichern Sie diese nach dem "least privilege" ab.
- Erstellen Sie sich einen **Loadbalancer** vor Ihrem ECS Service. Die IP soll statisch sein.
- Dokumentieren Sie den Ablauf für neue Versionen der ref-card-01, welche ECS Komponenten müssen aktualisiert werden? Dokumentieren Sie die benötigten Zeiten für den Neustart, wie lange sind mehrere Versionen aufrufbar? **Zusatzaufgabe**: Automatisieren Sie diesen Ablauf mit der **AWS CLI**.