# 20.3 Amazon Elastic Container Service

!!! abstract "Zielsetzung"

    Sie erstellen einen **Amazon Elastic Container Service Cluster**, erstellen eine **Task Definition** sowie einen **Service** um Ihre Container im Internet bereitzustellen.

## Konzept

Amazon Elastic Container Service (ECS) ist vergleichbar mit  [Kubernetes](https://kubernetes.io/), [Docker Swarm](https://docs.docker.com/swarm/overview/), and [Azure Container Service](https://azuremarketplace.microsoft.com/en-us/marketplace/apps/microsoft.acs). 

ECS unterstützt zwei unterschiedliche Betriebsmodelle:

- **Fargate**: Diese Option ist serverless - Sie können Container betreiben, ohne dass Sie Ihre Infrastruktur verwalten müssen. Geeignet für kleine Applikationen, Applikationen mit kurzzeitig hohen Lasten oder schnell wechselnde Lasten
- **EC2**: Sie konfigurieren EC2-Instanzen (Virtuelle Machinen) in Ihrem Cluster und stellen Sie sie bereit, um Ihre Container auszuführen.
  Geeignet für Applikationen mit konstant hoher CPU und Speichernutzung, Preisoptimierung oder Applikationen mit hohem Specherbedarf.

!!! info

	Für diesen Auftrag verwenden wir den serverless Ansatz mit **Fargate**. ECS übernimmt hier die Installation von Containern, die Skalierung, die Überwachung und die Verwaltung sowohl über eine API als auch über die AWS Management Console. Die spezifische Ort, auf der ein Container läuft, und die Wartung aller Instanzen werden von der Plattform übernommen. Wir müssen uns nicht darum kümmern.

### Task Definition

Die Task Definition ist eine Vorlage, die beschreibt, welche Docker-Container ausgeführt werden sollen und Ihre Anwendung darstellt. In unserem Beispiel wären es ein `nginx-custom` Container. Es beschreibt die zu verwendenden Images, die nötige CPU Rechenleistung, Speicher, die Umgebungsvariablen, die freizugebenden Ports und die Interaktion der Container.

### Task

Aus einer Task Definition können Instanzen gestartet werden, welche die Container gemäss dieser Konfiguration beinhaltet. Aus einer Task Definition können beliebig viele identische Tasks erstellt werden.

### Service

Definiert die minimale und maximale Anzahl von Tasks einer Task Definition, die zu einem bestimmten Zeitpunkt ausgeführt werden, sowie die automatische Skalierung und das Loadbalancing. 

Falls die CPU durch den einzigen laufenden Task ausgelastet ist, kann der Service automatisch zusätzliche Tasks hinzufügen. Es erlaubt zudem die maximale Anzahl der Tasks zu begrenzen, die ausgeführt werden können. Dies hilft, die Kosten von AWS unter Kontrolle zu halten

### Cluster

Der Service muss seine Tasks nun irgendwo ausführen können, damit sie zugänglich sind. Er muss einem Cluster zugeordnet werden und der Containerverwaltungsdienst sorgt selbstständig dafür, dass er genügend ECS-Instanzen für Ihren Cluster bereitstellt.

## Umsetzung

1. In der **AWS Management Console**, suchen Sie den Service **Elastic Container Service**
   ![image-20230328111818801](./assets/image-20230328111818801.png)

2. Erstellen Sie unter **Cluster** mit **Create cluster** einen neuen Cluster. Verwenden Sie als **Cluster name** `fargate-cluster`
    ![image-20230403161231204](assets/image-20230403161231204.png)

    !!! warning "Regionsbeschränkungen Learner Lab"

        Alle Services im AWS Academy Learner Lab sind auf die Regionen **us-east-1** und **us-west-2** beschränkt. Verwenden Sie nur diese Regionen.

    !!! failure "There was an error while retrieving a list of namespaces"

        Amazon hat die Funktionalität **Namespaces** neu eingeführt. Das Learner Lab hat diese Neuerungen noch nicht in ihr Rechtemodel eingebaut, weshalb wir die benötigten `ListNamespaces` Rechte noch nicht besitzen. Für unseren ECS Cluster verwenden wir **keine** Namespaces, Sie können den Fehler ignorieren.

3. Wählen Sie links im Menü **Task definitions** und erstellen Sie eine neue Task Definition mittels **Create new task definition**. Verwenden Sie `nginx-custom` als **Cluster name** und als **Image URI** Ihre persönliche URL zum privaten Repository aus dem Abschnitt **Elastic Container Registry** (Beispiel: `12345EXAMPLE.dkr.ecr.us-east-1.amazonaws.com/nginx-custom:latest`)
    ![image-20230328225653495](./assets/image-20230328225653495.png)

4. Nachdem Sie mittels **Next** zum Teil **Configure environment, storage, monitoring, and tags** gelangt sind, stellen Sie sicher, dass Sie unter **Environment** `AWS Fargate (serverless`) verwenden, setzen Sie **CPU** auf `.25 vCPU` und Memory auf `.5 GB`.
    ![image-20230328225952160](./assets/image-20230328225952160.png)

    !!! warning "LabRole"

        **Task role** und **Task execution role** müssen zwingendermassen auf `LabRole` gestellt werden, Sie erhalten sonst Fehlermeldungen und der Cluster kann nicht erstellt werden.

5. Wählen Sie links im Menü **Cluster** und erstellen Sie einen neuen **Service**.

    ![image-20230328230140030](./assets/image-20230328230140030.png)

6. Als **Compute options** wählen Sie `Launch type` und belassen die restlichen Voreinstellungen.

    ![image-20230328230553080](./assets/image-20230328230553080.png)

7. **Application type** muss ein `Service` sein. Bei **Task definition** wählen Sie unter **Family** die zuvor erstellte **Task definition** `nginx-custom` mit  **Revision** `latest`. **Desired tasks** ist in unserem Fall `1`.

    ![image-20230328230854487](./assets/image-20230328230854487.png)

8. Erstellen Sie eine neue **Security Group** mit dem Namen `fargate-service` und einer Beschreibung. Erlauben Sie **Type** `HTTP` und **Source** `Anywhere`. 

    ![image-20230328231146193](./assets/image-20230328231146193.png)

    !!! example "Optional"

        Alternativ können Sie auch die **Default Security Group** verwenden. Erstellen Sie anschliessend im **[VPC Dashboard](https://us-east-1.console.aws.amazon.com/vpc/home?region=us-east-1#SecurityGroups:)** die nötige zusätzliche **Inbound Rule** - dies haben Sie im Modul M346 gelernt.

9. Erstellen Sie den Service nun mittels **Create** und warten bis der Service deployed wurde. Sie finden Ihre Public IP indem Sie den **Service** `fargate-service` wählen, in den Tab **Configuration and tasks** wechseln und dort den **Task** auswählen. Im Bereich **Configuration** wird Ihre Public IP angezeigt. Testen Sie ob der Container läuft.

    ![image-20230328231304257](./assets/image-20230328231304257.png)


!!! success "Zusammenfassung"
	Sie haben auf einer öffentlichen IP eine Instanz Ihres `nginx-custom` Images bereitgestellt. Dazu haben Sie in AWS einen Elastic Container Service Cluster, die Task Definition und einen Service erstellt.