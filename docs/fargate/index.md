# 20.0 Einleitung

!!! abstract "Lernziel"

    In diesem Dokument lernen Sie, eine containerisierte Applikation in Amazon Elastic Container Registry hochzuladen und in Amazon Elastic Container Service bereitzustellen.

| Produkte                                                     | AWS Service                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| <img src="./assets/ubuntu.png" alt="img" style="width:32px;" /> WSL2 Ubuntu Terminal | ![Arch_Amazon-Elastic-Container-Registry_32](./assets/ecr.png) Elastic Container Registry |
| <img src="./assets/docker.png" alt="img" style="width:32px;" /> Docker Desktop mit WSL2 Integration | ![Arch_Amazon-Elastic-Container-Service_32](./assets/ecs.png) Elastic Container Service |
| <img src="assets/vscode.png" alt="vscode" style="width:32px;" /> Visual Studio Code mit [Remote Development extension pack](https://aka.ms/vscode-remote/download/extension) | ![Arch_Elastic-Load-Balancing_32](assets/elb.png) Elastic Load Balancing |

| Abschnitt                                                    | Zeit        |
| ------------------------------------------------------------ | ----------- |
| - Arbeitsumgebung<br />- Elastic Container Registry<br />- Amazon Elastic Container Service<br />- Aktualisiertes Docker Image | 2 Lektionen |
| - Arbeitsauftrag Ref-Card-01                                 | 2 Lektionen |



1. Docker Build mit einer Demo Nginx Applikation
2. Hochladen in Amazon Elastic Container Registry (ECR)
3. Setup eines Amazon Elastic Container Service in AWS
4. Erstellen einer Amazon ECS task definitions um Docker Container auszuführen
5. Bereitstellen des Containers mittels Service

