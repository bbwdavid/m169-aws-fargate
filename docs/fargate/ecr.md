# 20.2 Amazon Elastic Container Registry

Im nächsten Schritt laden wir unser lokal erstelltes Image in die Amazon Elastic Container Registry, kurz ECR.

Dazu benötigen Sie Zugang zu Ihrem [AWS Academy Learner Lab](https://awsacademy.instructure.com). Starten Sie Ihr Lab und öffnen Sie die AWS Management Console.

1. In der AWS Management Console, suchen Sie den Service **Elastic Container Registry**
   ![image-20230328075011231](./assets/image-20230328075011231.png)
   
2. Wählen Sie **Get Started** um ein neues Repository zu erstellen
   ![image-20230328075521898](./assets/image-20230328075521898.png)
   
3. Wählen Sie als **Visibilitiy settings** `Private` und als **Repository name** `nginx-custom`. Alle anderen Einstellungen können Sie wie vorausgewählt belassen.
   ![image-20230328080643753](./assets/image-20230328080643753.png)

4. Wählen Sie Ihr soeben erstelltes Repository **nginx-custom** aus![image-20230328080908031](./assets/image-20230328080908031.png)

5. Rechts finden Sie **View push commands**. Es enthält personalisierte Befehle damit Sie sich authentifzieren und Ihr Image in Ihre Repository pushen zu können.
   ![image-20230328081413014](./assets/image-20230328081413014.png)
   
	!!! failure "Fehlende AWS CLI"
		Sie haben aktuell keine [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) installiert. Der Befehl `aws ecr` wird eine Fehlermeldung ausgeben wenn Sie ihn lokal ausführen.
	
		``` { .sh .no-copy }
		$ aws ecr get-login-password --region us-east-1
		bash: aws: command not found
		```
	
	!!! success "Lösung: Benutzen Sie die Learner Lab Console"
		Führen Sie den Befehl `aws ecr get-login-password --region us-east-1` direkt im **Learner Lab** in der **Console** aus und kopieren Sie den Token in die Zwischenablage.
		![image-20230328085121237](./assets/image-20230328085121237.png)
	
6. In Ihrem lokalen Terminal geben Sie nun den zweiten Teil des Befehls nach dem `|` ein.
   ![image-20230328081413014](./assets/image-20230330122926124.png)

	=== "Variante 1"
		Sie kopieren den Token aus dem **Learner Lab** mit einem `echo` vor den `docker login` Befehl.
		:warning: Ersetzen Sie `1234EXAMPLE.dkr.ecr.us-east-1.amazonaws.com` mit **Ihrer** URL.
		``` sh
		$ echo "<TOKEN>" | docker login --username AWS --password-stdin 1234EXAMPLE.dkr.ecr.us-east-1.amazonaws.com
		Succeeded
		```

	=== "Variante 2"
		Entfernen Sie das Argument `--password-stdin` aus dem `docker login` Befehl um die Passworteingabe interaktiv durchzuführen. :warning: Die Passworteingabe wird nicht angezeigt, fügen Sie den kopierten Token mit einem Rechtsklick ein und Bestätigen Sie mit ++enter++
		``` sh
		$ docker login --username AWS 1234EXAMPLE.dkr.ecr.us-east-1.amazonaws.com
		Password:
		Login Succeeded
		```
	
7. Führen Sie nun die Schritte 2, 3 und 4 aus der **Push commands for nginx-custom** Anweisung in Ihrer **Elastic Container Registry** lokal im Terminal aus. 
    ![image-20230328101925571](./assets/image-20230328101925571.png)

	```{ .sh .no-copy }
	$ echo <TOKEN> | docker login --username AWS --password-stdin 12345EXAMPLE.dkr.ecr.us-east-1.amazonaws.com
	WARNING! Your password will be stored unencrypted in /home/dominic/.docker/config.json.
	Configure a credential helper to remove this warning. See
	https://docs.docker.com/engine/reference/commandline/login/#credentials-store
	
	Login Succeeded
	
	$ docker build -t nginx-custom .
	[+] Building 0.4s (7/7) FINISHED                              
	=> [internal] load .dockerignore
	=> => transferring context: 2B
	=> [internal] load build definition from Dockerfile
	=> => transferring dockerfile: 109B
	=> [internal] load metadata for docker.io/library/nginx:alpine-slim
	=> [1/2] FROM docker.io/library/nginx:alpine-slim@sha256:294dc03dddc4e1ae4062fa379b977ab49cf4f022dd4af371cde9015a0abb9c28
	=> [internal] load build context
	=> => transferring context: 32B
	=> CACHED [2/2] COPY index.html /usr/share/nginx/html/index.html
	=> exporting to image
	=> => exporting layers
	=> => writing image sha256:f7ad6939dccca8f72a13d112d1e719f8b944bce4dc6d49156e1d6c37216e384a
	=> => naming to docker.io/library/nginx-custom 
	
	$ docker tag nginx-custom:latest 12345EXAMPLE.dkr.ecr.us-east-1.amazonaws.com/nginx-custom:latest
	
	$ docker push 12345EXAMPLE.dkr.ecr.us-east-1.amazonaws.com/nginx-custom:latest
	The push refers to repository [12345EXAMPLE.dkr.ecr.us-east-1.amazonaws.com/nginx-custom]
	75e45621f57c: Pushed 
	f1bee861c2ba: Pushed 
	c4d67a5827ca: Pushed 
	152a948bab3b: Pushed 
	5e59460a18a3: Pushed 
	d8a5a02a8c2d: Pushed 
	7cd52847ad77: Pushed 
	latest: digest: sha256:0c531097485d76eeecc21c7929e4ab537dd24bcf079f4c9c80a3f2be65f26802 size: 1775
	```

!!! success "Zusammenfassung"
	Sie haben Ihr Docker Image `nginx-custom` erfolgreich erstellt, mit `nginx-custom:latest` getaggt und in Ihr neues Private Repository in **Amazon Elastic Container Registry** hochgeladen.
	![image-20230328102346821](./assets/image-20230328102346821.png)



