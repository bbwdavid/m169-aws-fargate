# 20.1 Arbeitsumgebung

Für diesen Auftrag benötigen Sie:

* [AWS Academy Learner Lab](https://awsacademy.instructure.com)
* Windows mit WSL2 / Ubuntu / Mac
* Docker Desktop mit aktivierter WSL2 Integration

## Arbeitsverzeichnis
Starten Sie ein Terminal (WSL2 mit Ubuntu unter Windows) und erstellen Sie sich in Ihrem Homeverzeichnis ein neues Verzeichnis.

```sh
mkdir m169-aws-fargate
```
Wechseln Sie in dieses Verzeichnis
```sh
cd m169-aws-fargate
```

## Docker

Wir arbeiten mit dem `alpine-slim` Nginx-Image, welches wir mit einem `Dockerfile` personalisieren. Die Grundlagen haben Sie im Auftrag **10.6_Docker Images erstellen** erarbeitet.

Erstellen Sie die Datei `Dockerfile` (Gross-/Kleinschreibung beachten, keine Dateiendung)

```dockerfile title="Dockerfile"
--8<-- "Dockerfile"
```

Zusätzlich erstellen Sie sich die Datei `index.html`

```html title="index.html"
--8<-- "index.html"
```

Als erster Arbeitsschritt prüfen wir `docker build` und starten daraus einen Container

```sh
docker build -t nginx-custom:latest .
```

Überprüfen Sie Ihr erstelltes Image mit den bereits bekannten Befehlen `images` oder `inspect`, lassen Sie anschliessend Ihr Image lokal mit `docker run` laufen und stellen Sie eine fehlerfreie Ausführung sicher. Nginx läuft standardmässig auf **Port 80,** erweitern sie den Docker-Befehl mit den Anweisungen um diesen Port auf Ihrem Gerät verfügbar zu machen. Konsultieren Sie die [Nginx Docker Hub](https://hub.docker.com/_/nginx) Dokumentation falls Sie die Befehle nicht mehr kennen.

![image-20230327231649548](./assets/image-20230327231649548.png)

!!! success "Zusammenfassung"
	In einem für diesen Auftrag neu erstellten Verzeichnis haben Sie die zwei Dateien **Dockerfile** und **index.html** erstellt. Daraus haben Sie sich ein Docker Image erstellt und erfolgreich im Browser getestet.